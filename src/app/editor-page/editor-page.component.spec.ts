import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorPageComponent } from './editor-page.component';
import { RouterPanelComponent } from '../router-panel/router-panel.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('EditorPageComponent', () => {
  let component: EditorPageComponent;
  let fixture: ComponentFixture<EditorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [  FormsModule, HttpClientModule ],
      declarations: [ EditorPageComponent, RouterPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
