import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftronViewerComponent } from './softron-viewer.component';

describe('SoftronViewerComponent', () => {
  let component: SoftronViewerComponent;
  let fixture: ComponentFixture<SoftronViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoftronViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftronViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
