import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { RouterService } from '../router.service';
import { Observable, interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-softron-viewer',
  templateUrl: './softron-viewer.component.html',
  styleUrls: ['./softron-viewer.component.scss']
})
export class SoftronViewerComponent implements OnInit {
  @Input() destinationIndex = null;
  @Input() address = null;
  @Input() port = 8080;
  @Input() uniqueId = null;
  @Input() isSelected = true;
  @Input() isViewOnly = false;
  @ViewChild('recordNameInput', {static: false}) recordNameInputRef: ElementRef;
  recordingName = null;
  editingName = false;
  info = null;
  headers;
  constructor(public routerService: RouterService, private cdr: ChangeDetectorRef, private http: HttpClient,private route: ActivatedRoute, private api:ApiServiceService) {
    this.headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
  }

  get isRecording(){
    if(this.info) return this.info.is_recording;
    else return false;
  }

  get elapsed(){
    var date = new Date(null);
    if(!this.info || !this.info.elapsed_seconds) return null;
    date.setSeconds(this.info.elapsed_seconds);
    var timeString = date.toISOString().substr(11, 8);
    console.log() 
    return timeString 
  }

  get destination(){
    return this.routerService.destinations.find(d=> d.index == this.destinationIndex) || {};
  }
  get source(){
    return this.routerService.sources.find(s=> s.index == this.destination.source) ||{};
  }
  get thumbnail(){
    if(this.info)
      return `http://${this.address}:${this.port}/sources/${this.uniqueId}/thumbnail?time=${Math.round(Date.now()/1000)}`;
    return '';
  }
  getInfo(){
    this.http.get(`http://${this.address}:${this.port}/sources/${this.uniqueId}/info`).subscribe(
      (info) => this.info = info,
      (err) => { 
        console.log("Error: get info failed");
        setTimeout(window.location.reload.bind(window.location), 60 * 1000);
      }
    );
    this.http.get(`http://${this.address}:${this.port}/sources/${this.uniqueId}`).subscribe(
      (data) => this.recordingName = data["recording_name"],
      (err) => { console.log("Error: get name failed") }
    );
  }
  ngOnInit() {
    interval(1000).pipe().subscribe(res=>{
      this.cdr.detectChanges()
    })
    interval(1000).pipe().subscribe(res=>{
      this.getInfo();
    })

    this.route.queryParams.subscribe(params => {
      console.log("query params")
      console.log(params)
      if(params["viewonly"]) this.isViewOnly = params["viewonly"] == "true";
      this.cdr.detectChanges()
    });
  }

  startRecording(event){
    this.http.get(`http://${this.address}:${this.port}/sources/${this.uniqueId}/record`).subscribe(
      (data) => console.log,
      (err) => console.error
    );
  }
  stopRecording(event){
    this.http.get(`http://${this.address}:${this.port}/sources/${this.uniqueId}/stop`).subscribe(
      (data) => console.log,
      (err) => console.error
    );
  }

  editName(event){
    this.editingName = true;
    this.cdr.detectChanges()
    this.recordNameInputRef.nativeElement.value = this.recordingName;
    this.recordNameInputRef.nativeElement.focus();
  }

  onSourceFocusOut(event){
    // this.http.put(`http://${this.address}:${this.port}/sources/${this.uniqueId}/recording_name`, {recording_name: event.target.value}, {headers: this.headers}).subscribe(
    //   (data) => {
    //     this.editingName = false;
    //     this.recordingName = event.target.value;
    //   },
    //   (err) => console.error
    // );
    this.changeRecordingName(event.target.value).then(name=>{
      this.recordingName = name;
    }).catch(e=>{
      console.log(e);
    });
  }
  doBlur(event){
    console.log("Blur")
    event.target.blur();
  }

  async changeRecordingName(name){
    let response = await this.api.changeRecordingName(`http://${this.address}:${this.port}/sources/${this.uniqueId}/recording_name`, name);
    let data = await response.json();
    this.editingName = false;
    if(!data.success) throw new Error(data.error)
    return name
  }


}
