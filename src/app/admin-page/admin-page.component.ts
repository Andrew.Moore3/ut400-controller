import { Component, OnInit } from '@angular/core';
import { RouterService } from '../router.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HostListener } from '@angular/core';

let hosturl = environment.API_HOST

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
      console.log("unloading")
      if (this.unsavedData) {
          // display warning
          $event.returnValue =true;
      }
  }
  unsavedData = false;
  constructor(private http: HttpClient, public routerService: RouterService) {
  }

  ngOnInit() {
  }
  somethingChanged(event){
      this.unsavedData = true;
  }
  save(event){
    this.http.post(`${hosturl}/destinations`, { destinations: this.routerService.destinations })
    .subscribe((val) => {
      this.unsavedData = false;
    }, 
    (err) => {
       alert("Error: Save destinations failed");
       console.log(err)
    });
    this.http.post(`${hosturl}/sources`, { sources: this.routerService.sources })
    .subscribe(console.log, console.error);

  }
  canDeactivate(){
    if(!this.unsavedData || confirm("You have unsaved changes. Continue anyway?")) return true;
    return false;
  }

}
