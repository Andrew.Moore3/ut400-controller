import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.scss']
})
export class DestinationsComponent implements OnInit {
  controlledDestination: number = -1;
  controlledDestinations: number[]=[]
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {

    let subQueryParams = this.route.queryParams.subscribe(queryparams => {
      this.controlledDestinations = queryparams["dests"].split(",")
    });
    let subParams = this.route.params.subscribe(params => {
      this.controlledDestination = params["id"];
    });
  }

}
