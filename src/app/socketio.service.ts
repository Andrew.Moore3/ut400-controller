import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  socket;
  constructor() {   }
  setupSocketConnection() {
    this.socket = io(environment.API_HOST);
  }

  public getMessages = () => {

    return Observable.create((observer) => {
      this.socket.on('take', (message) => {
          observer.next(message);
      });
      this.socket.on('status', (message) => {
          observer.next(message);
      });
  });
}
}
