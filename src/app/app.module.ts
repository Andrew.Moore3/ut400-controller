import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterPanelComponent } from './router-panel/router-panel.component';
import { SocketioService } from './socketio.service';
import { RouterModule, Routes } from '@angular/router';
import { EditorPageComponent } from './editor-page/editor-page.component';
import { HomeComponent } from './home/home.component';
import { WebPageComponent } from './web-page/web-page.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { CanDeactivateGuard } from './can-deactivate.guard';
import { ScheduleComponent } from './schedule/schedule.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { SoftronViewerComponent } from './softron-viewer/softron-viewer.component';
import { ProducerPageComponent } from './producer-page/producer-page.component';
import { McrPageComponent } from './mcr-page/mcr-page.component'

const appRoutes: Routes = [
  { path: 'editor', component: EditorPageComponent },
  { path: 'web', component: WebPageComponent },
  { path: 'producer', component: ProducerPageComponent },
  { path: 'mcr', component: McrPageComponent },
  { path: 'admin', component: AdminPageComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'schedule', component: ScheduleComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'destinations', component: DestinationsComponent},
  { path: 'destinations/:id', component: DestinationsComponent},
  { path: 'weather', redirectTo: '/destinations?dests=51,52,53,54',  pathMatch: 'full'},
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    RouterPanelComponent,
    EditorPageComponent,
    HomeComponent,
    WebPageComponent,
    AdminPageComponent,
    ScheduleComponent,
    DestinationsComponent,
    SoftronViewerComponent,
    ProducerPageComponent,
    McrPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  providers: [SocketioService, CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
