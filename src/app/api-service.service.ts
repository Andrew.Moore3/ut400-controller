import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  constructor() { }
  changeRecordingName(url, name){
    return fetch(url, {
      method: "PUT",
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({recording_name: name})
    });
  }
}
