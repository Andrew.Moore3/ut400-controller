import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McrPageComponent } from './mcr-page.component';

describe('McrPageComponent', () => {
  let component: McrPageComponent;
  let fixture: ComponentFixture<McrPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McrPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McrPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
