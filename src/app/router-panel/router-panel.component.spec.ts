import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterPanelComponent } from './router-panel.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('RouterPanelComponent', () => {
  let component: RouterPanelComponent;
  let fixture: ComponentFixture<RouterPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [  FormsModule, HttpClientModule ],
      declarations: [ RouterPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should display "Missing Destination" for no detination', () =>{
    component.destinationDisabled = true;
    component.selectedDestinationIndex = -1;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".title").textContent).toContain("Missing Destination")
  });
  it('should display "select a destination" for when panel is variable', () =>{
    component.destinationDisabled = false;
    component.selectedDestinationIndex = 61;
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector(".title").textContent).not.toContain("Missing Destination")
  });

  it('should have a take button', () =>{
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll(".myButton")[0].textContent).toContain("Take")
  });

  it('should have a status button', () =>{
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll(".myButton")[1].textContent).toContain("Get Status")
  });
});
