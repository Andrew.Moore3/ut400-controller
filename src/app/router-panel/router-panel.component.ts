import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RouterService } from '../router.service';

let basevalue = 230;
let range = 10;
let hosturl = environment.API_HOST

@Component({
  selector: 'app-router-panel',
  templateUrl: './router-panel.component.html',
  styleUrls: ['./router-panel.component.scss']
})
export class RouterPanelComponent implements OnInit {
  @Input() selectedDestinationIndex = -1;
  @Input() destinationDisabled: boolean = false;
  backgroundColor = `rgb(${basevalue + Math.random() * range}, ${basevalue + Math.random() * range}, ${basevalue + Math.random() * range})`;

  constructor(private http: HttpClient, public routerService: RouterService) {
  }

  // selectedSourceIndex = null;
  get selectedDestination() {
    return this.routerService.destinations.find(d => d.index == this.selectedDestinationIndex) || {};
  }
  get selectedSource() {
    return this.routerService.sources.find(s => s.index == this.selectedSourceIndex) || {};
  }

  get selectedSourceIndex() {
    let destination = this.routerService.destinations.find(d => d.index == this.selectedDestinationIndex);
    return destination ? destination.source : null;
  }
  set selectedSourceIndex(value) {
    this.selectedDestination.source = value;
  }

  ngOnInit() {
  }

  onDestinationChange(event) {
    //   this.selectedSourceIndex = this.selectedDestination.source;
  }
  onSourceChange(event) {
    //   this.selectedDestination.source = this.selectedSourceIndex;
  }
  onSourceFocus(event) {
    console.log("CLICK")
    this.selectedDestination.source = null;
  }
  take(event) {
    console.log(`TAKE ${this.selectedDestinationIndex} -> ${this.selectedSourceIndex}`);
    if (!this.selectedDestinationIndex || !this.selectedSourceIndex) return alert("Please select a destination and source")
    return this.http.post(`${hosturl}/destinations/${this.selectedDestinationIndex}`, { source: this.selectedSourceIndex }).subscribe((val) => console.log(val), (err) => { alert("Error: Take failed") });
  }

  status(event) {
    console.log(`STATUS ${this.selectedDestinationIndex}`);
    if (!this.selectedDestinationIndex) return alert("Please select a destination")
    return this.http.get(`${hosturl}/destinations/${this.selectedDestinationIndex}`).subscribe(
      (res) => this.selectedSourceIndex = res["data"].index,
      (err) => { alert("Error: Status failed") }
    );
  }
}
