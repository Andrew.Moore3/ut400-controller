import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare const Twitch: any;

@Component({
  selector: 'app-producer-page',
  templateUrl: './producer-page.component.html',
  styleUrls: ['./producer-page.component.scss']
})
export class ProducerPageComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  public types = [
    { value: 'obs', display: 'No Delay/No Sound' },
    { value: 'hls', display: 'Some delay with sound' }
  ];
  playerType: string = "obs";
  ngOnInit() {
    var options = {
      width: "100%",
      height: "100%",
      channel: "abc10news"
    };
    console.log(this.route.queryParams);
    // this.playerType = queryparams["type"]; //"obs" or "hls"

    let subQueryParams = this.route.queryParams.subscribe(queryparams => {
      console.log(queryparams);
      this.playerType = queryparams["type"] || this.playerType; //"obs" or "hls"
    });
  }
}
