import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SocketioService } from './socketio.service';
import { environment } from 'src/environments/environment';
import { RouterService } from './router.service';

let hosturl = environment.API_HOST

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ut400-controller';
  constructor(private http: HttpClient, private socketService: SocketioService, private routerService: RouterService){
    console.log(`host: ${hosturl}`)
  }

  ngOnInit(){
    let obsDestinations = this.http.get(`${hosturl}/destinations`);
    let obsSources = this.http.get(`${hosturl}/sources`);
    obsDestinations.subscribe((destinations: any)=>{
       this.routerService.destinations = destinations.data
       console.log(destinations) 
    });
    obsSources.subscribe((sources:any)=>{ this.routerService.sources = sources.data });
    this.socketService.setupSocketConnection();

    this.socketService
      .getMessages()
      .subscribe((message: string) => {
        let destId = parseInt(message.split(":")[0])
        let destination = this.routerService.destinations.find(d=>d.index == destId);
        if(!destination) return console.log(`No destination found with index: ${destId}`)
        destination.source = parseInt(message.split(":")[1]);
        console.log(message)
      });
  }
}
